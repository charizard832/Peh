package com.charizard832.game;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.charizard832.game.LegendGame;
import com.charizard832.gameworld.GameWorld;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class AndroidLauncher extends AndroidApplication implements ActionResolver{

	private InterstitialAd mInterstitialAd;
	private RewardedVideoAd mAd;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		initialize(new LegendGame(this), config);

		MobileAds.initialize(this, "ca-app-pub-3435145263390263~2671531433");
		mAd = MobileAds.getRewardedVideoAdInstance(this);
		mAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
			@Override
			public void onRewardedVideoAdLoaded() {
				test = true;
			}

			@Override
			public void onRewardedVideoAdOpened() {

			}

			@Override
			public void onRewardedVideoStarted() {

			}

			@Override
			public void onRewardedVideoAdClosed() {
				test = false;
				loadRewardedVideoAd();
			}

			@Override
			public void onRewarded(RewardItem rewardItem) {

				// call rewards method from here.
				GameWorld.rewardPehbbles();
				loadRewardedVideoAd();  // Load for next Reward Point
			}

			@Override
			public void onRewardedVideoAdLeftApplication() {

			}

			@Override
			public void onRewardedVideoAdFailedToLoad(int i) {
				System.out.print("Failed to load video");
				loadRewardedVideoAd();
				test = false;
			}
		});
		loadRewardedVideoAd();

		mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId("ca-app-pub-3435145263390263/5624997835");
		mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("057AA0F1628ED924BD2AB48A366192F4").addTestDevice("895FFF37FE79176E2CD72F12109AE2ED").build());
		showOrLoadInterstital();
		//MobileAds.initialize(this, "ca-app-pub-3435145263390263/5624997835");
	}
	public void showOrLoadInterstital(){
		try {
			runOnUiThread(new Runnable() {
				public void run() {
					if (mInterstitialAd.isLoaded()) {
						mInterstitialAd.show();
					}
					else {
						AdRequest interstitialRequest = new AdRequest.Builder().build();
						mInterstitialAd.loadAd(interstitialRequest);
					}
				}
			});
		} catch (Exception e) {
		}
	}


	public void showVideoAd(){
		runOnUiThread(new Runnable() {
			public void run() {

				if (mAd.isLoaded()) {
					mAd.show();
				} else {
					loadRewardedVideoAd();
				}
			}
		});
	}

	boolean test;
	public boolean hasVideoReward(){
		if(test)
			return true;
		runOnUiThread(new Runnable() {
			public void run() {
				if(!mAd.isLoaded())
					loadRewardedVideoAd();
			}
		});
		return false;
	}
	private void loadRewardedVideoAd() {
		mAd.loadAd("ca-app-pub-3435145263390263/7285724587", new AdRequest.Builder().build());

	}

	@Override
	protected void onResume() {
		super.onResume();
		mAd.resume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mAd.pause(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mAd.destroy(this);
	}
	public void initVideoAd(){}
}
