package com.charizard832.shop.categories.powerups;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.ShopCategory;
import com.charizard832.shop.categories.ShopEntry;

/**
 * Created by zlnor on 11/23/2017.
 */

public class PowerupsCategory extends ShopCategory{

    public PowerupsCategory(){
        super(new SimpleButton(93, 28, 40, 15, AssetLoader.powerupsButtonUp, AssetLoader.powerupsButtonDown), new ShopEntry[]{});
    }

    @Override
    public void render(SpriteBatch batch, BitmapFont font){
        
    }

}
