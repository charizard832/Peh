package com.charizard832.shop.categories.skins;

import com.charizard832.game.Data;
import com.charizard832.helpers.AssetLoader;

/**
 * Created by zlnor on 11/3/2017.
 */

public class BluePehSkin extends PurchaseShopEntry {
    public BluePehSkin(){
        super(AssetLoader.legend, "Blue Peh", 0, Data.BLUEPEH);
    }


    public boolean buy(){
        return false;
    }
    public void activate(){
        Data.setActiveSkin(0);
    }
}
