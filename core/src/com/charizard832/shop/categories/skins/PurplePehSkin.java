package com.charizard832.shop.categories.skins;

import com.charizard832.game.Data;
import com.charizard832.gameworld.GameWorld;
import com.charizard832.helpers.AssetLoader;

/**
 * Created by zlnor on 11/7/2017.
 */

public class PurplePehSkin extends PurchaseShopEntry {
    public PurplePehSkin(){
        super(AssetLoader.purplePeh, "Purple Peh", 100, Data.PURPLEPEH);
    }


    public boolean buy(){
        if(Data.PEHBBLES>= initCost){
            Data.PEHBBLES-= initCost;
            GameWorld.prefs.putInteger("pehbbles", Data.PEHBBLES);
            Data.PURPLEPEH = 1;
            GameWorld.prefs.putInteger("purplepeh", Data.PURPLEPEH);
            GameWorld.prefs.flush();
            return true;
        }
        return false;
    }
    public void activate(){
        Data.setActiveSkin(3);
    }
}
