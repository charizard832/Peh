package com.charizard832.shop.categories.skins;

import com.charizard832.game.Data;
import com.charizard832.gameworld.GameWorld;
import com.charizard832.helpers.AssetLoader;

/**
 * Created by zlnor on 11/3/2017.
 */

public class RedPehSkin extends PurchaseShopEntry {

    public RedPehSkin(){
        super(AssetLoader.redPeh, "Red Peh", 100, Data.REDPEH);
    }


    public boolean buy(){
        if(Data.PEHBBLES>= initCost){
            Data.PEHBBLES-= initCost;
            GameWorld.prefs.putInteger("pehbbles", Data.PEHBBLES);
            Data.REDPEH = 1;
            GameWorld.prefs.putInteger("redpeh", Data.REDPEH);
            GameWorld.prefs.flush();
            return true;
        }
        return false;
    }
    public void activate(){
        Data.setActiveSkin(1);
    }

}
