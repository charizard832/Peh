package com.charizard832.shop.categories.skins;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.charizard832.gameobjects.buttons.shopbuttons.PurchaseButton;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.ShopEntry;

/**
 * Created by zlnor on 11/24/2017.
 */

public abstract class PurchaseShopEntry extends ShopEntry {

    protected boolean bought;

    public PurchaseShopEntry(TextureRegion image, String name, int initCost, int data){
        super(image, name, initCost, data);
        buyButton = new PurchaseButton(this);
        switch(data){
            case 1:
                buyButton.setTextures(AssetLoader.activateButtonUp, AssetLoader.activateButtonDown);
                break;
            case 2:
                buyButton.setTextures(AssetLoader.active);
                break;
        }
        if(data>0){
            bought();
            ((PurchaseButton)buyButton).setBought(bought);
        }
    }
    public abstract void activate();
    public boolean isBought(){return bought;}
    public void bought(){bought = true;}
}
