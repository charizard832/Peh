package com.charizard832.shop.categories.skins;

import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.gameobjects.buttons.shopbuttons.PurchaseButton;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.ShopCategory;
import com.charizard832.shop.categories.ShopEntry;

import java.util.ArrayList;

/**
 * Created by zlnor on 11/3/2017.
 */

public class SkinsCategory extends ShopCategory {

    public SkinsCategory(){
        super(new SimpleButton(/*5*/22, /*28*/28, 40, 15, AssetLoader.skinsButtonUp, AssetLoader.skinsButtonDown), new ShopEntry[]{new BluePehSkin(), new RedPehSkin(), new YellowPehSkin(), new PurplePehSkin()});

    }

}
