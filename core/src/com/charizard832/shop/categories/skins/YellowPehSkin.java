package com.charizard832.shop.categories.skins;

import com.charizard832.game.Data;
import com.charizard832.gameworld.GameWorld;
import com.charizard832.helpers.AssetLoader;

/**
 * Created by zlnor on 11/7/2017.
 */

public class YellowPehSkin extends PurchaseShopEntry {
    public YellowPehSkin(){
        super(AssetLoader.yellowPeh, "Yellow Peh", 100, Data.YELLOWPEH);
    }


    public boolean buy(){
        if(Data.PEHBBLES>= initCost){
            Data.PEHBBLES-= initCost;
            GameWorld.prefs.putInteger("pehbbles", Data.PEHBBLES);
            Data.YELLOWPEH = 1;
            GameWorld.prefs.putInteger("yellowpeh", Data.YELLOWPEH);
            GameWorld.prefs.flush();
            return true;
        }
        return false;
    }
    public void activate(){
        Data.setActiveSkin(2);
    }
}
