package com.charizard832.shop.categories.upgrades;

import com.charizard832.game.Data;
import com.charizard832.helpers.AssetLoader;

/**
 * Created by zlnor on 12/3/2017.
 */

public class PehbblesUpgrade extends UpgradeShopEntry {

    public PehbblesUpgrade(){
        super(AssetLoader.pehbble, "Pehbbles Given", 100, Data.PEHBBLESGIVEN, 3, 3.5f, "pehbblesgiven", 0.45f, 17, 10);
    }

    public boolean buy(){
        Data.PEHBBLESGIVEN++;
        return false;
    }
}
