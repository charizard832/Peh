package com.charizard832.shop.categories.upgrades;

import com.charizard832.game.Data;
import com.charizard832.helpers.AssetLoader;

/**
 * Created by zlnor on 11/24/2017.
 */

public class PehLivesUpgrade extends UpgradeShopEntry{

    public PehLivesUpgrade(){
        super(AssetLoader.legend, "Start Lives", 200, Data.UPGRADELIVES, 4, 1.5f, "upgradelives", 0.58f);
    }

    public boolean buy(){
        Data.UPGRADELIVES++;
        return false;
    }

}
