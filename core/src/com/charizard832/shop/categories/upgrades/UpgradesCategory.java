package com.charizard832.shop.categories.upgrades;

import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.ShopCategory;
import com.charizard832.shop.categories.ShopEntry;

/**
 * Created by zlnor on 11/23/2017.
 */

public class UpgradesCategory extends ShopCategory{

    public UpgradesCategory(){
        super(new SimpleButton(/*49*/75, /*28*/28, 40, 15, AssetLoader.upgradesButtonUp, AssetLoader.upgradesButtonDown), new ShopEntry[]{new PehLivesUpgrade(), new SunLivesUpgrade(), new PehbblesUpgrade()});
    }
}
