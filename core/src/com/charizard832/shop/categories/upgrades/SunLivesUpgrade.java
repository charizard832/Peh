package com.charizard832.shop.categories.upgrades;

import com.charizard832.game.Data;
import com.charizard832.helpers.AssetLoader;

/**
 * Created by zlnor on 12/3/2017.
 */

public class SunLivesUpgrade extends UpgradeShopEntry {
    public SunLivesUpgrade(){
        super(AssetLoader.sun, "Lives Sun Gives", 1000, Data.SUNLIVES, 2, 2f, "sunlives", 0.425f);
    }

    public boolean buy(){
        Data.SUNLIVES++;
        return false;
    }
}
