package com.charizard832.shop.categories.upgrades;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.charizard832.game.Data;
import com.charizard832.gameobjects.buttons.shopbuttons.UpgradeButton;
import com.charizard832.gameworld.GameWorld;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.ShopEntry;

/**
 * Created by zlnor on 11/24/2017.
 */

public abstract class UpgradeShopEntry extends ShopEntry {

    protected int upgradeCount, upgradeCountMax, imageWidth, imageHeight;
    protected float costMultiplier, nameScale;
    protected String dataId;

    public UpgradeShopEntry(TextureRegion image, String name, int initCost, int data, int upgradeCountMax, float costMultiplier, String dataId, float nameScale){
        super(image, name, initCost, data);
        this.upgradeCountMax = upgradeCountMax;
        this.costMultiplier = costMultiplier;
        this.dataId = dataId;
        this.nameScale = nameScale;
        upgradeCount = data;
        buyButton = new UpgradeButton(this);
        if(upgradeCount == upgradeCountMax)
            buyButton.setTextures(AssetLoader.maxed);
        cost = (int)((double)initCost * Math.pow(costMultiplier, data));
        imageWidth = 17;
        imageHeight = 17;
    }
    public UpgradeShopEntry(TextureRegion image, String name, int initCost, int data, int upgradeCountMax, float costMultiplier, String dataId, float nameScale, int imageWidth, int imageHeight){
        super(image, name, initCost, data);
        this.upgradeCountMax = upgradeCountMax;
        this.costMultiplier = costMultiplier;
        this.dataId = dataId;
        this.nameScale = nameScale;
        upgradeCount = data;
        buyButton = new UpgradeButton(this);
        if(upgradeCount == upgradeCountMax)
            buyButton.setTextures(AssetLoader.maxed);
        cost = (int)((double)initCost * Math.pow(costMultiplier, data));
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
    }

    public boolean upgrade(){
        if(upgradeCount == upgradeCountMax)
            return true;
        if(Data.PEHBBLES>=cost && upgradeCount<upgradeCountMax) {
            upgradeCount++;
            Data.PEHBBLES-=cost;
            GameWorld.prefs.putInteger("pehbbles", Data.PEHBBLES);
            GameWorld.prefs.putInteger(dataId, upgradeCount);
            GameWorld.prefs.flush();
            cost = (int)((float)cost*costMultiplier);
            buy();
            if(upgradeCount == upgradeCountMax)
                return true;
        }
        return false;
    }


    public int getImageWidth(){return imageWidth;}
    public int getImageHeight(){return imageHeight;}
    public int getUpgradeCount(){return upgradeCount;}
    public int getUpgradeCountMax(){return upgradeCountMax;}
    public float getNameScale(){return nameScale;}

}
