package com.charizard832.shop.categories;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.gameobjects.buttons.shopbuttons.PurchaseButton;
import com.charizard832.helpers.AssetLoader;

/**
 * Created by zlnor on 11/1/2017.
 */

public abstract class ShopEntry {

    protected TextureRegion image;
    protected String name;
    protected int initCost, cost;
    protected SimpleButton buyButton;
    protected int data;

    public ShopEntry(TextureRegion image, String name, int initCost, int data){
        this.image = image;
        this.name = name;
        this.initCost = initCost;
        cost = initCost;
        this.data = data;
    }

    public abstract boolean buy();



    public TextureRegion getImage(){return image;}
    public String getName(){return name;}
    public int getInitCost(){return initCost;}
    public SimpleButton getBuyButton(){return buyButton;}
    public int getData(){return data;}
    public int getCost(){return cost;}


}
