package com.charizard832.shop.categories;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.upgrades.UpgradeShopEntry;

/**
 * Created by zlnor on 11/1/2017.
 */

public abstract class ShopCategory {

    private SimpleButton categoryButton;
    protected ShopEntry[] entries;

    public ShopCategory(SimpleButton categoryButton, ShopEntry[] entries){
        this.categoryButton = categoryButton;
        this.entries = entries;
        fixButtonY();
    }


    public SimpleButton getCategoryButton(){return categoryButton;}
    public ShopEntry[] getEntries(){return entries;}

    private void fixButtonY(){
        for(int i = 0; i < entries.length; i++) {
            ShopEntry temp = entries[i];
            temp.getBuyButton().setY(i*35+47);
        }
    }

    public void render(SpriteBatch batch, BitmapFont font){
        for(int i = 0; i < entries.length; i++){
            ShopEntry temp = entries[i];
            if(temp instanceof com.charizard832.shop.categories.skins.PurchaseShopEntry) {
                batch.draw(temp.getImage(), 5, i * 35 + 45);
                font.getData().setScale(0.58f, 0.58f);
                font.draw(batch, temp.getName(), 30, i * 35 + 50);
                temp.getBuyButton().draw(batch);
                if (((com.charizard832.shop.categories.skins.PurchaseShopEntry) temp).isBought())
                    font.draw(batch, "Bought", 5, i * 35 + 65);
                else
                    font.draw(batch, "Cost: " + temp.getCost() + " pehbbles", 5, i * 35 + 65);
            }else if(temp instanceof com.charizard832.shop.categories.upgrades.UpgradeShopEntry){
                batch.draw(temp.getImage(), 5, i * 35 + 45, ((UpgradeShopEntry) temp).getImageWidth(), ((UpgradeShopEntry) temp).getImageHeight());
                font.getData().setScale(((UpgradeShopEntry) temp).getNameScale(), ((UpgradeShopEntry) temp).getNameScale());
                font.draw(batch, temp.getName(), 27, i * 35 + 50);
                temp.getBuyButton().draw(batch);
                font.getData().setScale(0.45f, 0.45f);
                if(((UpgradeShopEntry) temp).getUpgradeCount()<((UpgradeShopEntry) temp).getUpgradeCountMax())
                    font.draw(batch, "Cost: " + temp.getCost(), 92, i * 35 + 63);
                else
                    font.draw(batch, "MAXED", 95, i * 35 + 63);

                for(int k = 0; k < ((com.charizard832.shop.categories.upgrades.UpgradeShopEntry) temp).getUpgradeCountMax(); k++){
                    if(k<((com.charizard832.shop.categories.upgrades.UpgradeShopEntry) temp).getUpgradeCount())
                        batch.draw(AssetLoader.upgradeSlotPurchased, k*15+28, i*35 + 63);
                    else
                        batch.draw(AssetLoader.upgradeSlot, k*15+28, i*35 + 63);
                }
            }

        }
    }
}
