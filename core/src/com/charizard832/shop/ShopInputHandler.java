package com.charizard832.shop;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.charizard832.game.ActionResolver;
import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.gameobjects.buttons.shopbuttons.PurchaseButton;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.skins.PurchaseShopEntry;
import com.charizard832.shop.categories.ShopCategory;
import com.charizard832.shop.categories.ShopEntry;
import com.charizard832.shop.categories.upgrades.UpgradeShopEntry;

/**
 * Created by zlnor on 10/25/2017.
 */

public class ShopInputHandler {

    private Shop shop;

    private float scaleFactorX, scaleFactorY;
    private int midPointX, midPointY;

    private SimpleButton backButton, videoButton;
    private ActionResolver ar;

    public ShopInputHandler(float scaleFactorX, float scaleFactorY, int midPointX, int midPointY, ActionResolver ar){
        this.scaleFactorX = scaleFactorX;
        this.scaleFactorY = scaleFactorY;
        this.midPointX = midPointX;
        this.midPointY = midPointY;
        this.ar = ar;
        backButton = new SimpleButton(5, midPointY+67, 25, 25, AssetLoader.backButtonUp, AssetLoader.backButtonDown);
        videoButton = new SimpleButton(45, midPointY+67, 69, 21, AssetLoader.videoUp, AssetLoader.videoDown);
    }

    public void touchDown(int screenX, int screenY){
        for(ShopCategory c : shop.getCategories()){
            c.getCategoryButton().isTouchDown(screenX, screenY);
            for(ShopEntry e: c.getEntries()){
                e.getBuyButton().isTouchDown(screenX, screenY);
            }
        }
        backButton.isTouchDown(screenX, screenY);
        videoButton.isTouchDown(screenX, screenY);
    }
    public boolean touchUp(int screenX, int screenY){
        for(ShopCategory c : shop.getCategories()){
            if(c.getCategoryButton().isTouchUp(screenX, screenY)){
                shop.setActiveCategory(c);
            }
            for(ShopEntry e: c.getEntries()){
                if(e instanceof PurchaseShopEntry && shop.getActiveCategoryNumber() == 0) {
                    if (e.getBuyButton().buy(screenX, screenY)) {
                        for (ShopEntry e2 : c.getEntries()) {
                            if (e2 != e && ((PurchaseShopEntry) e2).isBought()) {
                                ((PurchaseButton) e2.getBuyButton()).deactivate();
                            }
                        }
                    }
                } else if(e instanceof UpgradeShopEntry && shop.getActiveCategoryNumber() == 1){
                    e.getBuyButton().buy(screenX, screenY);
                }
            }
        }
        if(backButton.isTouchUp(screenX, screenY))
            return false;
        if(videoButton.isTouchUp(screenX, screenY)){
            if(ar.hasVideoReward())
                ar.showVideoAd();
        }
        return true;
    }

    public void setShop(Shop shop){this.shop = shop;}
    public SimpleButton getBackButton(){return backButton;}
    public SimpleButton getVideoButton(){return videoButton;}
}
