package com.charizard832.shop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.charizard832.game.ActionResolver;
import com.charizard832.game.Data;
import com.charizard832.gameobjects.Legend;
import com.charizard832.gameworld.GameRenderer;
import com.charizard832.gameworld.GameWorld;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.helpers.InputHandler;
import com.charizard832.shop.categories.ShopCategory;
import com.charizard832.shop.categories.powerups.PowerupsCategory;
import com.charizard832.shop.categories.skins.SkinsCategory;
import com.charizard832.shop.categories.upgrades.UpgradesCategory;

import java.util.ArrayList;

/**
 * Created by zlnor on 10/25/2017.
 */

public class Shop {

    private int midPointY, midPointX;

    private Legend legend;

    private GameWorld world;
    private GameRenderer gameRenderer;

    private OrthographicCamera cam;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch batcher;
    private BitmapFont font;

    private ShopInputHandler inputHandler;

    private ArrayList<ShopCategory> categories;
    private int activeCategory;

    public Shop(GameWorld world, GameRenderer gameRenderer, int midPointY, int midPointX){
        this.world = world;
        this.gameRenderer = gameRenderer;
        this.midPointY = midPointY;
        this.midPointX = midPointX;

        this.cam = gameRenderer.getCam();
        this.shapeRenderer = gameRenderer.getShapeRenderer();
        this.batcher = gameRenderer.getBatcher();
        this.font = gameRenderer.getFont();
        font.getData().setScale(1.2f, 1.2f);

        inputHandler = ((InputHandler) Gdx.input.getInputProcessor()).getShopInputHandler();

        legend = world.getLegend();

        categories = new ArrayList<ShopCategory>();
        categories.add(new SkinsCategory());
        categories.add(new UpgradesCategory());
       // categories.add(new PowerupsCategory());

        activeCategory = 0;
        categories.get(0).getCategoryButton().setTextures(AssetLoader.skinsButtonDown);
    }

    public void update(float delta){
    }

    public void render(float delta, float runtime){
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(55 / 255.0f, 180 / 255.0f, 255 / 255.0f, 1);
        shapeRenderer.rect(0, 0, 136, world.getGameHeight());
        shapeRenderer.setColor(0f, 0f, 0f, 1);
        shapeRenderer.rect(0, 25, 136, 2);
        shapeRenderer.end();
        batcher.begin();
        font.getData().setScale(1.2f, 1.2f);
        font.draw(batcher, "SHOP      :", 5, 5);
        font.getData().setScale(0.9f, 0.9f);
        font.draw(batcher, ""+Data.PEHBBLES, 95, 7);
        batcher.draw(AssetLoader.pehbble, 60, 6, AssetLoader.pehbble.getRegionWidth()*2, AssetLoader.pehbble.getRegionHeight()*2);
        font.getData().setScale(0.6f, 0.6f);
        //font.draw(batcher, "More coming", 62, midPointY+72);
        //font.draw(batcher, "soon!", 62, midPointY+81);
        for(ShopCategory c : categories)
            c.getCategoryButton().draw(batcher);
        categories.get(activeCategory).render(batcher, font);
        inputHandler.getBackButton().draw(batcher);
        inputHandler.getVideoButton().draw(batcher);
        batcher.end();
    }

    public ArrayList<ShopCategory> getCategories(){return categories;}
    public ShopCategory getActiveCategory(int i){return categories.get(i);}
    public int getActiveCategoryNumber(){return activeCategory;}
    public void setActiveCategory(ShopCategory category){
        if(category instanceof SkinsCategory){
            activeCategory = 0;
            categories.get(0).getCategoryButton().setTextures(AssetLoader.skinsButtonDown);
            categories.get(1).getCategoryButton().setTextures(AssetLoader.upgradesButtonUp, AssetLoader.upgradesButtonDown);
            //categories.get(2).getCategoryButton().setTextures(AssetLoader.powerupsButtonUp, AssetLoader.powerupsButtonDown);
        }
        if(category instanceof UpgradesCategory){
            activeCategory = 1;
            categories.get(0).getCategoryButton().setTextures(AssetLoader.skinsButtonUp, AssetLoader.skinsButtonDown);
            categories.get(1).getCategoryButton().setTextures(AssetLoader.upgradesButtonDown);
            //categories.get(2).getCategoryButton().setTextures(AssetLoader.powerupsButtonUp, AssetLoader.powerupsButtonDown);
        }
        if(category instanceof  PowerupsCategory){
            activeCategory = 2;
            categories.get(0).getCategoryButton().setTextures(AssetLoader.skinsButtonUp, AssetLoader.skinsButtonDown);
            categories.get(1).getCategoryButton().setTextures(AssetLoader.upgradesButtonUp, AssetLoader.upgradesButtonDown);
            //categories.get(2).getCategoryButton().setTextures(AssetLoader.powerupsButtonDown);
        }
    }

}
