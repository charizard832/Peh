package com.charizard832.gameobjects.buttons.shopbuttons;

import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.upgrades.UpgradeShopEntry;

/**
 * Created by zlnor on 11/1/2017.
 */

public class UpgradeButton extends SimpleButton {

    private UpgradeShopEntry shopEntry;

    public UpgradeButton(UpgradeShopEntry shopEntry){
        super(95, 0, AssetLoader.upgradeButtonDown.getRegionWidth(), AssetLoader.upgradeButtonDown.getRegionHeight(), AssetLoader.upgradeButtonUp, AssetLoader.upgradeButtonDown);
        this.shopEntry = shopEntry;
    }

    public boolean buy(int screenX, int screenY){
        if(isTouchUp(screenX, screenY))
            if(shopEntry.upgrade())
                setTextures(AssetLoader.maxed);
        return true;
    }

}
