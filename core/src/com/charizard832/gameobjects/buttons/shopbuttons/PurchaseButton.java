package com.charizard832.gameobjects.buttons.shopbuttons;

import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.shop.categories.skins.PurchaseShopEntry;

/**
 * Created by zlnor on 11/1/2017.
 */

public class PurchaseButton extends SimpleButton {

    private PurchaseShopEntry shopEntry;
    private boolean bought, active;

    public PurchaseButton(PurchaseShopEntry shopEntry){
        super(90, 0, AssetLoader.purchaseButtonDown.getRegionWidth(), AssetLoader.purchaseButtonDown.getRegionHeight(), AssetLoader.purchaseButtonUp, AssetLoader.purchaseButtonDown);
        this.shopEntry = shopEntry;
        bought = shopEntry.isBought();
        active = false;
    }

    public boolean buy(int screenX, int screenY){
        if(!active) {
            if (isTouchUp(screenX, screenY)) {
                if (bought) {
                    setTextures(AssetLoader.active);
                    active = true;
                    shopEntry.activate();
                    return true;
                } else {
                    if (shopEntry.buy()) {
                        setTextures(AssetLoader.activateButtonUp, AssetLoader.activateButtonDown);
                        bought = true;
                        shopEntry.bought();
                    }
                }
            }
        }
        return false;
    }

    public void deactivate(){setTextures(AssetLoader.activateButtonUp, AssetLoader.activateButtonDown);active = false;}
    public void setBought(boolean bought){this.bought = bought;}
}
