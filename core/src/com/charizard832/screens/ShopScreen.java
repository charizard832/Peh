package com.charizard832.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.charizard832.game.ActionResolver;
import com.charizard832.gameworld.GameRenderer;
import com.charizard832.gameworld.GameWorld;
import com.charizard832.shop.Shop;

/**
 * Created by zlnor on 10/25/2017.
 */

public class ShopScreen implements Screen {

    private Shop shop;

    private float runtime;

    public ShopScreen(GameWorld gameWorld, GameRenderer gameRenderer){
        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();
        float gameWidth = 136;
        float gameHeight = screenHeight / (screenWidth / gameWidth);

        int midPointY = (int) (gameHeight / 2);
        int midPointX = (int) (gameWidth / 2);

        shop = new Shop(gameWorld, gameRenderer, midPointY, midPointX);
    }

    public void render(float delta){
        runtime+=delta;
        shop.update(delta);
        shop.render(delta, runtime);
    }

    public Shop getShop(){return shop;}

    public void resize(int a, int b){

    }

    public void resume(){

    }

    public void pause(){

    }

    public void hide(){

    }

    public void show(){

    }

    public void dispose(){

    }
}
