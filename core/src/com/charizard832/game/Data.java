package com.charizard832.game;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.charizard832.gameworld.GameWorld;
import com.charizard832.helpers.AssetLoader;
import com.charizard832.helpers.InputHandler;

/**
 * Created by chari on 8/5/2016.
 */
public class Data {

    public static int SCORE = 0;
    public static int LIVES = 1;
    public static int MAXLIVES = 1;
    public static int HIGHSCORE = 0;
    public static int PEHBBLES = 0;
    public static int ACTIVESKIN = 0;

    public static int BLUEPEH = 2, REDPEH = 0, YELLOWPEH = 0, PURPLEPEH = 0;

    public static int UPGRADELIVES = 0;
    public static int SUNLIVES = 0;
    public static int PEHBBLESGIVEN = 0;

    public static boolean SOUNDMUTED = false;

    public static TextureRegion activePehTexture = AssetLoader.legend;
    public static TextureRegion activePehFadeTexture = AssetLoader.legendFade;
    public static Animation<TextureRegion> activePehRightAnimation = AssetLoader.legendAnimationRight;
    public static Animation<TextureRegion> activePehLeftAnimation = AssetLoader.legendAnimationLeft;


    /*
    0- Blue Peh
    1- Red Peh
    2- Yellow Peh
    */
    public static void setActiveSkin(int skin){
        ACTIVESKIN = skin;
        GameWorld.prefs.putInteger("activeskin", ACTIVESKIN);
        switch(ACTIVESKIN){
            case 0:
                activePehTexture = AssetLoader.legend;
                activePehFadeTexture = AssetLoader.legendFade;
                activePehRightAnimation = AssetLoader.legendAnimationRight;
                activePehLeftAnimation = AssetLoader.legendAnimationLeft;
                InputHandler.gameButtons.get(0).setTextures(AssetLoader.legendFade);
                InputHandler.gameButtons.get(1).setTextures(AssetLoader.legend);
                BLUEPEH = 2;
                if(REDPEH>0)
                    REDPEH = 1;
                if(YELLOWPEH>0)
                    YELLOWPEH = 1;
                if(PURPLEPEH>0)
                    PURPLEPEH = 1;
                break;
            case 1:
                activePehTexture = AssetLoader.redPeh;
                activePehFadeTexture = AssetLoader.redPehFade;
                activePehRightAnimation = AssetLoader.redPehAnimationRight;
                activePehLeftAnimation = AssetLoader.redPehAnimationLeft;
                InputHandler.gameButtons.get(0).setTextures(AssetLoader.redPehFade);
                InputHandler.gameButtons.get(1).setTextures(AssetLoader.redPeh);
                BLUEPEH = 1;
                if(REDPEH>0)
                    REDPEH = 2;
                if(YELLOWPEH>0)
                    YELLOWPEH = 1;
                if(PURPLEPEH>0)
                    PURPLEPEH = 1;
                break;
            case 2:
                activePehTexture = AssetLoader.yellowPeh;
                activePehFadeTexture = AssetLoader.yellowPehFade;
                activePehRightAnimation = AssetLoader.yellowPehAnimationRight;
                activePehLeftAnimation = AssetLoader.yellowPehAnimationLeft;
                InputHandler.gameButtons.get(0).setTextures(AssetLoader.yellowPehFade);
                InputHandler.gameButtons.get(1).setTextures(AssetLoader.yellowPeh);
                BLUEPEH = 1;
                if(REDPEH>0)
                    REDPEH = 1;
                if(YELLOWPEH>0)
                    YELLOWPEH = 2;
                if(PURPLEPEH>0)
                    PURPLEPEH = 1;
                break;
            case 3:
                activePehTexture = AssetLoader.purplePeh;
                activePehFadeTexture = AssetLoader.purplePehFade;
                activePehRightAnimation = AssetLoader.purplePehAnimationRight;
                activePehLeftAnimation = AssetLoader.purplePehAnimationLeft;
                InputHandler.gameButtons.get(0).setTextures(AssetLoader.purplePehFade);
                InputHandler.gameButtons.get(1).setTextures(AssetLoader.purplePeh);
                BLUEPEH = 1;
                if(REDPEH>0)
                    REDPEH = 1;
                if(YELLOWPEH>0)
                    YELLOWPEH = 1;
                if(PURPLEPEH>0)
                    PURPLEPEH = 2;
                break;
        }
        GameWorld.prefs.putInteger("bluepeh", BLUEPEH);
        GameWorld.prefs.putInteger("redpeh", REDPEH);
        GameWorld.prefs.putInteger("yellowpeh", YELLOWPEH);
        GameWorld.prefs.putInteger("purplepeh", PURPLEPEH);
        GameWorld.prefs.flush();
    }

}
