package com.charizard832.game;

/**
 * Created by zlnor on 8/9/2017.
 */

public interface ActionResolver {

    public void showOrLoadInterstital();
    public void showVideoAd();
    public boolean hasVideoReward();
    public void initVideoAd();
}
