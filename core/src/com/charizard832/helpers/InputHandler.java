package com.charizard832.helpers;

import java.util.List;
import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.InputProcessor;
import com.charizard832.game.ActionResolver;
import com.charizard832.game.Data;
import com.charizard832.game.LegendGame;
import com.charizard832.gameobjects.Legend;
import com.charizard832.gameobjects.LegendFade;
import com.charizard832.gameobjects.buttons.SimpleButton;
import com.charizard832.gameworld.GameRenderer;
import com.charizard832.gameworld.GameWorld;
import com.charizard832.screens.GameScreen;
import com.charizard832.screens.ShopScreen;
import com.charizard832.screens.TutorialScreen;
import com.charizard832.shop.ShopInputHandler;

/**
 * Created by chari on 6/17/2016.
 */
public class InputHandler implements InputProcessor {

    private Legend legend;
    private GameWorld world;
    private float scaleFactorX;
    private float scaleFactorY;
    private int midPointX;
    private int midPointY;

    private List<SimpleButton> menuButtons, shopButtons;
    public static List<SimpleButton> gameButtons;
    private SimpleButton playButton, fade, fadeSnap, tutButton, pauseButton, shopButton, soundButton;

    private LegendFade lf;
    public boolean dir, beginDir;

    private GameScreen gs;

    private boolean tutorial, shop;

    private TutorialScreen ts;

    private ShopScreen ss;
    private ShopInputHandler shopInputHandler;
    public InputHandler(GameWorld world, float scaleFactorX,
                        float scaleFactorY, int midPointX, int midPointY, GameScreen gs, ActionResolver ar) {

        ts = null;
        ss = null;
        this.gs = gs;

        tutorial = false;
        shop = false;

        dir = false;
        beginDir = true;

        this.midPointX = midPointX;
        this.midPointY = midPointY;
        // int midPointY = world.getMidPointY();

        menuButtons = new ArrayList<SimpleButton>();
        gameButtons = new ArrayList<SimpleButton>();
        shopButtons = new ArrayList<SimpleButton>();

        playButton = new SimpleButton(136/2 - (AssetLoader.playButtonDown.getRegionWidth()), midPointY, 27*2, 21*2, AssetLoader.playButtonUp, AssetLoader.playButtonDown);
        fade = new SimpleButton(136/2 - (AssetLoader.legend.getRegionWidth())+60, midPointY+10, 20, 20, AssetLoader.legendFade, AssetLoader.legendFade);
        fadeSnap = new SimpleButton(136/2 - (AssetLoader.legend.getRegionWidth())+60, midPointY+30, 20, 20, AssetLoader.legend, AssetLoader.legend);
        tutButton = new SimpleButton(136/2 - (AssetLoader.tutButtonDown.getRegionWidth()/2), midPointY+30+AssetLoader.playButtonDown.getRegionHeight(), 34, 9, AssetLoader.tutButtonUp, AssetLoader.tutButtonDown);
        pauseButton = new SimpleButton(136/2 - (AssetLoader.pauseButtonDown.getRegionWidth())/2+57, 5, 15, 17, AssetLoader.pauseButtonUp, AssetLoader.pauseButtonDown);
        shopButton = new SimpleButton(136/2 - (AssetLoader.shopButtonDown.getRegionWidth())/2, midPointY+65, AssetLoader.shopButtonDown.getRegionWidth(), AssetLoader.shopButtonUp.getRegionHeight(), AssetLoader.shopButtonUp, AssetLoader.shopButtonDown);
        if(Data.SOUNDMUTED)
            soundButton = new SimpleButton(136/2 - (AssetLoader.pauseButtonDown.getRegionWidth())/2+57, 25, 15, 17, AssetLoader.soundMutedButtonUp, AssetLoader.soundMutedButtonDown);
        else
            soundButton = new SimpleButton(136/2 - (AssetLoader.pauseButtonDown.getRegionWidth())/2+57, 25, 15, 17, AssetLoader.muteSoundButtonUp, AssetLoader.muteSoundButtonDown);

        menuButtons.add(playButton);
        menuButtons.add(tutButton);
        menuButtons.add(shopButton);
        gameButtons.add(fade);
        gameButtons.add(fadeSnap);
        gameButtons.add(pauseButton);
        gameButtons.add(soundButton);

        this.scaleFactorX = scaleFactorX;
        this.scaleFactorY = scaleFactorY;

        this.world = world;
        this.legend = world.getLegend();

        lf = new LegendFade(world, legend.getX(), legend.getY(), 17, 17);

        shopInputHandler = new ShopInputHandler(scaleFactorX, scaleFactorY, midPointX, midPointY, ar);
    }

    public LegendFade getLf(){return lf;};


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        screenX = scaleX(screenX);
        screenY = scaleY(screenY);
        if(shop) {
            shopInputHandler.touchDown(screenX, screenY);
        }else if(!tutorial) {
                switch (world.getGameState()) {
                    case GAMEOVER:
                    case READY:
                        playButton.isTouchDown(screenX, screenY);
                        tutButton.isTouchDown(screenX, screenY);
                        shopButton.isTouchDown(screenX, screenY);
                        soundButton.isTouchDown(screenX, screenY);
                        break;
                    case RUNNING:
                        if (fade.isTouchDown(screenX, screenY)) {
                            lf.setX((int) legend.getX());
                            return true;
                        }
                        if (fadeSnap.isTouchDown(screenX, screenY)) {
                            legend.setX((int) lf.getX());
                            return true;
                        }
                        if(!pauseButton.isTouchDown(screenX, screenY)&&!soundButton.isTouchDown(screenX, screenY)) {
                            if (beginDir) {
                                beginDir = false;
                                if (screenX > legend.getX()) {
                                    dir = false;
                                    legend.setVelocity(legend.getSpeedX(), 0);
                                    legend.setRotation(1);
                                } else {
                                    dir = true;
                                    legend.setVelocity(-legend.getSpeedX(), 0);
                                    legend.setRotation(2);
                                }
                            } else {
                                if (!legend.isStopped()) {
                                    if (dir) {
                                        dir = false;
                                        legend.setVelocity(legend.getSpeedX(), 0);
                                        legend.setRotation(1);
                                    } else {
                                        dir = true;
                                        legend.setVelocity(-legend.getSpeedX(), 0);
                                        legend.setRotation(2);
                                    }
                                } else {
                                    legend.setStopped(false);
                                    if (screenX > legend.getX()) {
                                        dir = false;
                                        legend.setVelocity(legend.getSpeedX(), 0);
                                        legend.setRotation(1);
                                    } else {
                                        dir = true;
                                        legend.setVelocity(-legend.getSpeedX(), 0);
                                        legend.setRotation(2);
                                    }
                                }
                            }
                        }
                        break;
                }
            }else{
                switch (ts.getTut().getFlag()){
                    case 0:
                    case 1:
                        ts.getTut().progress();
                        break;
                    case 2:
                        ts.getTut().progress();
                        if (screenX > legend.getX()) {
                            legend.setVelocity(legend.getSpeedX(), 0);
                            legend.setRotation(1);
                        } else {
                            legend.setVelocity(-legend.getSpeedX(), 0);
                            legend.setRotation(2);
                        }
                        break;
                    case 3:
                    case 4:
                        ts.getTut().progress();
                        break;
                    case 5:
                        ts.getTut().progress();
                        if (midPointX > legend.getX()) {
                            legend.setVelocity(legend.getSpeedX(), 0);
                            legend.setRotation(1);
                        } else {
                            legend.setVelocity(-legend.getSpeedX(), 0);
                            legend.setRotation(2);
                        }
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                        ts.getTut().progress();
                        break;
                    case 12:
                        playButton.isTouchDown(screenX, screenY);
                        break;
                }
            }
        return true;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        screenX = scaleX(screenX);
        screenY = scaleY(screenY);

        if(shop){
            shop = shopInputHandler.touchUp(screenX, screenY);
            if(!shop){
                gs.setGameScreen();
            }
        } else if(tutorial){
            if (playButton.isTouchUp(screenX, screenY)) {
                ts.dispose();
                tutorial = false;
                world.reset();
                gs.setGameScreen();
            }
        }
        switch(world.getGameState()){
            case GAMEOVER:
                if(playButton.isTouchUp(screenX, screenY)){
                    world.reset();
                    lf.setX((int)legend.getX());
                    beginDir = true;
                }
                if(soundButton.isTouchUp(screenX, screenY)){
                    if(Data.SOUNDMUTED){
                        AssetLoader.ded.play();
                        soundButton.setTextures(AssetLoader.muteSoundButtonUp, AssetLoader.muteSoundButtonDown);
                    }else{
                        AssetLoader.ded.stop();
                        soundButton.setTextures(AssetLoader.soundMutedButtonUp, AssetLoader.soundMutedButtonDown);
                    }
                    Data.SOUNDMUTED = !Data.SOUNDMUTED;
                    GameWorld.prefs.putBoolean("soundmuted", Data.SOUNDMUTED);
                    GameWorld.prefs.flush();
                }
            case READY:
                if(playButton.isTouchUp(screenX, screenY)){
                    world.start();
                    lf.setX((int)legend.getX());
                    beginDir = true;
                    legend.setVelocity(0, 0);
                }
                if(tutButton.isTouchUp(screenX, screenY)) {
                    ts = gs.setTutorialScreen();
                    tutorial = true;
                }
                if(shopButton.isTouchUp(screenX, screenY)){
                    ss = gs.setShopScreen();
                    shopInputHandler.setShop(ss.getShop());
                    shop = true;
                }
                break;
            case RUNNING:
                if(pauseButton.isTouchUp(screenX, screenY)){
                    if(!GameWorld.paused) {
                        pauseButton.setTextures(AssetLoader.playButton2Up, AssetLoader.playButton2Down);
                        AssetLoader.bg.pause();
                    }else{
                        pauseButton.setTextures(AssetLoader.pauseButtonUp, AssetLoader.pauseButtonDown);
                        if(!Data.SOUNDMUTED)
                            AssetLoader.bg.play();
                    }
                    GameWorld.paused = !GameWorld.paused;
                }
                if(soundButton.isTouchUp(screenX, screenY)){
                    if(Data.SOUNDMUTED){
                        AssetLoader.bg.play();
                        soundButton.setTextures(AssetLoader.muteSoundButtonUp, AssetLoader.muteSoundButtonDown);
                    }else{
                        AssetLoader.bg.stop();
                        soundButton.setTextures(AssetLoader.soundMutedButtonUp, AssetLoader.soundMutedButtonDown);
                    }
                    Data.SOUNDMUTED = !Data.SOUNDMUTED;
                    GameWorld.prefs.putBoolean("soundmuted", Data.SOUNDMUTED);
                    GameWorld.prefs.flush();
                }
                break;
        }
        return true;
    }
    private int scaleX(int screenX) {
        return (int) (screenX / scaleFactorX);
    }

    private int scaleY(int screenY) {
        return (int) (screenY / scaleFactorY);
    }
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public List<SimpleButton> getMenuButtons(){return menuButtons;}
    public List<SimpleButton> getGameButtons(){return gameButtons;}
    public ShopInputHandler getShopInputHandler(){return shopInputHandler;}

}
