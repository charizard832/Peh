package com.charizard832.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by chari on 6/17/2016.
 */
public class AssetLoader {

    public static Texture texture;
    //public static TextureRegion bg, grass;

    public static Animation<TextureRegion> legendAnimationRight, legendAnimationLeft;
    public static TextureRegion legend, legendDown, legendRight, legendLeft, legendFade;

    public static TextureRegion sun, pehbble;

    public static TextureRegion enemy1;

    public static TextureRegion peh;

    public static TextureRegion
            playButtonUp, playButtonDown,
            tutButtonUp, tutButtonDown,
            pauseButtonUp, pauseButtonDown,
            playButton2Up, playButton2Down,
            shopButtonUp, shopButtonDown,
            upgradeButtonUp, upgradeButtonDown,
            purchaseButtonUp, purchaseButtonDown,
            activateButtonUp, activateButtonDown,
            skinsButtonUp, skinsButtonDown,
            upgradesButtonUp, upgradesButtonDown,
            powerupsButtonUp, powerupsButtonDown,
            backButtonUp, backButtonDown,
            muteSoundButtonUp, muteSoundButtonDown,
            soundMutedButtonUp, soundMutedButtonDown,
            videoUp, videoDown
    ;

    public static TextureRegion maxed, active, upgradeSlot, upgradeSlotPurchased;


    //skins//////////////////////////////////////////////////////////
    public static TextureRegion redPeh, redPehDown, redPehRight, redPehLeft, redPehFade;
    public static Animation<TextureRegion> redPehAnimationRight, redPehAnimationLeft;

    public static TextureRegion yellowPeh, yellowPehDown, yellowPehRight, yellowPehLeft, yellowPehFade;
    public static Animation<TextureRegion> yellowPehAnimationRight, yellowPehAnimationLeft;
    
    public static TextureRegion purplePeh, purplePehDown, purplePehRight, purplePehLeft, purplePehFade;
    public static Animation<TextureRegion> purplePehAnimationRight, purplePehAnimationLeft;
///////////////////////////////////////////////////////////////////////
    public static Music bg, ded;

    public static void load() {

        bg = Gdx.audio.newMusic(Gdx.files.internal("peh test 2.wav"));
        bg.setLooping(true);

        ded = Gdx.audio.newMusic(Gdx.files.internal("peh ded.wav"));
        ded.setLooping(true);

        texture = new Texture(Gdx.files.internal("texture.png"));
        texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        legend = createRegion(0, 0, 17, 17);
        legendDown = createRegion(34, 0, 17, 17);
        legendRight = createRegion( 17, 0, 17, 17);
        legendLeft = createRegion(51, 0, 17, 17);
        legendFade = createRegion(37, 17, 17, 17);

        sun = createRegion(0, 17, 37, 36);
        pehbble = createRegion(68, 0, 12, 7);
        enemy1 = createRegion(0, 53, 20, 35);

        peh = createRegion(80, 0, 84, 26);

        playButtonUp = createRegion(165, 0, 27, 21);
        playButtonDown = createRegion(192, 0, 27, 21);

        tutButtonUp = createRegion(165, 21, 34, 9);
        tutButtonDown = createRegion(165, 30, 34, 9);

        playButton2Up = createRegion(20, 53, 15, 17);
        playButton2Down = createRegion(35, 53, 15, 17);

        pauseButtonUp = createRegion(50, 53, 15, 17);
        pauseButtonDown = createRegion(65, 53, 15, 17);

        shopButtonUp = createRegion(219, 0, 37, 15);
        shopButtonDown = createRegion(219, 15, 37, 15);

        upgradeButtonUp = createRegion(218, 30, 38, 14);
        upgradeButtonDown = createRegion(218, 44, 38, 14);
        maxed = createRegion(215, 85, 41, 14);

        purchaseButtonUp = createRegion(212, 57, 44, 14);
        purchaseButtonDown = createRegion(212, 71, 44, 14);

        activateButtonUp = createRegion(209, 113, 47, 14);
        activateButtonDown = createRegion(209, 127, 47, 14);
        active = createRegion(220, 99, 36, 14);

        skinsButtonUp = createRegion(134, 27, 31, 14);
        skinsButtonDown = createRegion(134, 41, 31, 14);

        upgradesButtonUp = createRegion(122, 55, 43, 14);
        upgradesButtonDown = createRegion(122, 69, 43, 14);

        powerupsButtonUp = createRegion(123, 83, 42, 14);
        powerupsButtonDown = createRegion(123, 97, 42, 14);

        backButtonUp = createRegion(165, 39, 22, 21);
        backButtonDown = createRegion(187, 39, 22, 21);

        upgradeSlot = createRegion(199, 21, 13, 6);
        upgradeSlotPurchased = createRegion(199, 27, 13, 6);

        muteSoundButtonUp = createRegion(20, 70, 15, 17);
        muteSoundButtonDown = createRegion(35, 70, 15, 17);

        soundMutedButtonUp = createRegion(50, 70, 15, 17);
        soundMutedButtonDown = createRegion(65, 70, 15, 17);

        videoUp = createRegion(165, 60, 46, 14);
        videoDown = createRegion(165, 74, 46, 14);


        TextureRegion[] legendsRight = { legend, legendRight, legendDown, legendLeft };
        legendAnimationRight = new Animation<TextureRegion>(0.09f, legendsRight);
        legendAnimationRight.setPlayMode(Animation.PlayMode.LOOP);

        TextureRegion[] legendsLeft = { legend, legendLeft, legendDown, legendRight };
        legendAnimationLeft = new Animation<TextureRegion>(0.09f, legendsLeft);
        legendAnimationLeft.setPlayMode(Animation.PlayMode.LOOP);

        //skins//////////////////////////////
        redPeh = createRegion(0, 88, 17, 17);
        redPehRight = createRegion(17, 88, 17, 17);
        redPehDown = createRegion(34, 88, 17, 17);
        redPehLeft = createRegion(51, 88, 17, 17);
        redPehFade = createRegion(68, 88, 17, 17);

        TextureRegion[] redPehsRight = {redPeh, redPehRight, redPehDown, redPehLeft};
        redPehAnimationRight = new Animation<TextureRegion>(0.09f, redPehsRight);
        redPehAnimationRight.setPlayMode(Animation.PlayMode.LOOP);

        TextureRegion[] redPehsLeft = {redPeh, redPehLeft, redPehDown, redPehRight};
        redPehAnimationLeft = new Animation<TextureRegion>(0.09f, redPehsLeft);
        redPehAnimationLeft.setPlayMode(Animation.PlayMode.LOOP);


        yellowPeh = createRegion(0, 105, 17, 17);
        yellowPehRight = createRegion(17, 105, 17, 17);
        yellowPehDown = createRegion(34, 105, 17, 17);
        yellowPehLeft = createRegion(51, 105, 17, 17);
        yellowPehFade = createRegion(68, 105, 17, 17);

        TextureRegion[] yellowPehsRight = {yellowPeh, yellowPehRight, yellowPehDown, yellowPehLeft};
        yellowPehAnimationRight = new Animation<TextureRegion>(0.09f, yellowPehsRight);
        yellowPehAnimationRight.setPlayMode(Animation.PlayMode.LOOP);

        TextureRegion[] yellowPehsLeft = {yellowPeh, yellowPehLeft, yellowPehDown, yellowPehRight};
        yellowPehAnimationLeft = new Animation<TextureRegion>(0.09f, yellowPehsLeft);
        yellowPehAnimationLeft.setPlayMode(Animation.PlayMode.LOOP);

        purplePeh = createRegion(0, 122, 17, 17);
        purplePehRight = createRegion(17, 122, 17, 17);
        purplePehDown = createRegion(34, 122, 17, 17);
        purplePehLeft = createRegion(51, 122, 17, 17);
        purplePehFade = createRegion(68, 122, 17, 17);

        TextureRegion[] purplePehsRight = {purplePeh, purplePehRight, purplePehDown, purplePehLeft};
        purplePehAnimationRight = new Animation<TextureRegion>(0.09f, purplePehsRight);
        purplePehAnimationRight.setPlayMode(Animation.PlayMode.LOOP);

        TextureRegion[] purplePehsLeft = {purplePeh, purplePehLeft, purplePehDown, purplePehRight};
        purplePehAnimationLeft = new Animation<TextureRegion>(0.09f, purplePehsLeft);
        purplePehAnimationLeft.setPlayMode(Animation.PlayMode.LOOP);
////////////////////////////////////////////////////////////

    }

    private static TextureRegion createRegion(int x, int y, int width, int height){
        TextureRegion region = new TextureRegion(texture, x, y, width, height);
        region.flip(false, true);
        return region;
    }

    public static void dispose() {
        // We must dispose of the texture when we are finished.
        bg.dispose();
        ded.dispose();
        texture.dispose();
    }

}
