package com.charizard832.game;

import com.badlogic.gdx.backends.iosmoe.IOSApplication;
import com.badlogic.gdx.backends.iosmoe.IOSApplicationConfiguration;

import org.moe.binding.googlemobileads.GADAdReward;
import org.moe.binding.googlemobileads.GADInterstitial;
import org.moe.binding.googlemobileads.GADMobileAds;
import org.moe.binding.googlemobileads.GADRequest;
import org.moe.binding.googlemobileads.GADRewardBasedVideoAd;
import org.moe.binding.googlemobileads.c.GoogleMobileAds;
import org.moe.binding.googlemobileads.protocol.GADRewardBasedVideoAdDelegate;
import org.moe.natj.general.Pointer;
import com.charizard832.game.LegendGame;
import com.charizard832.gameworld.GameWorld;

import apple.uikit.UIApplication;
import apple.uikit.UIViewController;
import apple.uikit.c.UIKit;

public class IOSMoeLauncher extends IOSApplication.Delegate implements ActionResolver, GADRewardBasedVideoAdDelegate{

    private GADInterstitial interstitial;
    private GADRewardBasedVideoAd rbAd;
    private boolean initialized = false;



    private IOSApplication app;
    protected IOSMoeLauncher(Pointer peer) {
        super(peer);
    }

    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();
        config.useAccelerometer = false;
        //showOrLoadInterstital();
        app = new IOSApplication(new LegendGame(this), config);
        return app;
    }

    public static void main(String[] argv) {
        UIKit.UIApplicationMain(0, null, null, IOSMoeLauncher.class.getName());
    }
    public void showOrLoadInterstital(){
        if(!initialized){
            initialized = true;
            GADMobileAds.configureWithApplicationID("ca-app-pub-3435145263390263~3303245685");
            interstitial = GADInterstitial.alloc().initWithAdUnitID("ca-app-pub-3435145263390263/8619027837");
            GADRequest request = GADRequest.request();
            interstitial.loadRequest(request);
        }else{
            if (interstitial.isReady()) {
                interstitial.presentFromRootViewController(app.getUIViewController());
            } else {
                interstitial = GADInterstitial.alloc().initWithAdUnitID("ca-app-pub-3435145263390263/8619027837");
                GADRequest request = GADRequest.request();
                interstitial.loadRequest(request);
            }
        }
    }

    @Override
    public void applicationDidBecomeActive(UIApplication application){
        super.applicationDidBecomeActive(application);
        showOrLoadInterstital();

    }


    public void showVideoAd(){
        if(rbAd.isReady()) {
            rbAd.presentFromRootViewController(app.getUIViewController());
            GameWorld.rewardPehbbles();
        }else
            loadRewardedVideoAd();
    }

    public boolean hasVideoReward(){
        if(!rbAd.isReady()){
            loadRewardedVideoAd();
        }
        return rbAd.isReady();
    }
    private void loadRewardedVideoAd() {
        rbAd.setDelegate_unsafe(this);
        rbAd.loadRequestWithAdUnitID(GADRequest.request(), "ca-app-pub-3435145263390263/1857976381");


        if(rbAd.isReady()) {
            rbAd.presentFromRootViewController(app.getUIViewController());
            GameWorld.rewardPehbbles();
        }
    }

    public void initVideoAd(){
        GADRewardBasedVideoAd.initialize();
        rbAd = GADRewardBasedVideoAd.alloc();
        rbAd.init();
        rbAd.setDelegate(this);
        rbAd.loadRequestWithAdUnitID(GADRequest.request(), "ca-app-pub-3435145263390263/1857976381");

    }
    @Override
    public void rewardBasedVideoAdDidRewardUserWithReward(GADRewardBasedVideoAd rewardBasedVideoAd, GADAdReward reward) {

        GameWorld.rewardPehbbles();
    }

    @Override
    public void rewardBasedVideoAdDidClose(
            GADRewardBasedVideoAd rewardBasedVideoAd) {
        GameWorld.rewardPehbbles();
    }
}
